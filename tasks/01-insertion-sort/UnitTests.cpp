#include <test_runner.h>
#include <random_traits.h>
#include <algorithm>
#include <numeric>

#include <CountingDouble.h>

#include "InsertionSort.h"

using std::vector;

void CorrectnessTest0() {
  vector<int> v = {8, 7, 6, 5, 4, 3, 2, 1};
  const vector<int> expected = {1, 2, 3, 4, 5, 6, 7, 8};

  InsertionSort(v);

  ASSERT_EQUAL(v, expected);
}

void ParametrizedTest(size_t n, int from, int to, int seed) {
  vector<int> v = testing::GenerateVector<int>(n, from, to, seed);
  const vector<int> expected = [&]{
    auto e = v;
    std::sort(e.begin(), e.end());
    return e;
  }();

  InsertionSort(v);

  ASSERT_EQUAL(v, expected);
}

void CorrectnessTest1() {
  ParametrizedTest(20, -100, 100, 42);
}

void CorrectnessTest2() {
  ParametrizedTest(99, -1000, 1000, 43);
}

// Count assignments of numbers
// If the amount is wrong the algorithm is either wrong or unoptimal
void CorrectAlgoTest() {
  vector<CountingDouble> v = {1, 2, 4, 5, 7, 8, 6, 3};

  const vector<CountingDouble> expected = {1, 2, 3, 4, 5, 6, 7, 8};

  InsertionSort(v);

  ASSERT_EQUAL(v, expected);

  size_t assignments = 0;
  for(auto x: v) {
    assignments += x.Assignments();
  }

  ASSERT_EQUAL(assignments, 14);
}

void StressTest0() {
  ParametrizedTest(50000, -200000, 200000, 1337);
}

void StressTest1() {
  ParametrizedTest(60000, -4000000, 4000000, 1338);
}

void ParametrizedStress(size_t n) {
  vector<int> v(n);
  iota(v.rbegin(), v.rend(), 1);

  // https://www.cppstories.com/2016/11/iife-for-complex-initialization/
  const vector<int> expected = [n]{
    vector<int> v(n);
    iota(v.begin(), v.end(), 1);
    return v;
  }();

  InsertionSort(v);

  ASSERT_EQUAL(v, expected);
}

void StressTest2() {
  ParametrizedStress(50000);
}

void StressTest3() {
  ParametrizedStress(60000);
}

void StressTest4() {
  ParametrizedStress(100000);
}

void RunAllTests() {
  using namespace std::chrono;
  testing::TestRunner tr;

  RUN_TEST(tr, CorrectnessTest0);
  RUN_TEST(tr, CorrectnessTest1);
  RUN_TEST(tr, CorrectnessTest2);

  RUN_TEST(tr, CorrectAlgoTest);

  RUN_TIMEOUT_TEST(tr, StressTest0, seconds(1));
  RUN_TIMEOUT_TEST(tr, StressTest1, seconds(1));
  RUN_TIMEOUT_TEST(tr, StressTest2, seconds(2));
  RUN_TIMEOUT_TEST(tr, StressTest3, seconds(3));
  RUN_TIMEOUT_TEST(tr, StressTest4, seconds(6));
}

int main() {
  RunAllTests();
}
