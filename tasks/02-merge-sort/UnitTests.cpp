#include <test_runner.h>
#include <random_traits.h>
#include <algorithm>
#include <numeric>

#include "MergeSort.h"

using std::vector;

void CorrectnessTest0() {
  vector<int> v = {8, 7, 6, 5, 4, 3, 2, 1};
  const vector<int> expected = {1, 2, 3, 4, 5, 6, 7, 8};

  MergeSort(v);

  ASSERT_EQUAL(v, expected);
}

void ParametrizedTest(size_t n, int from, int to, int seed) {
  vector<int> v = testing::GenerateVector<int>(n, from, to, seed);
  const vector<int> expected = [&]{
    auto e = v;
    std::sort(e.begin(), e.end());
    return e;
  }();

  MergeSort(v);

  ASSERT_EQUAL(v, expected);
}

void CorrectnessTest1() {
  ParametrizedTest(20, -100, 100, 42);
}

void CorrectnessTest2() {
  ParametrizedTest(99, -1000, 1000, 43);
}

void StressTest0() {
  ParametrizedTest(250000, -200000, 200000, 1337);
}

void StressTest1() {
  ParametrizedTest(300000, -4000000, 4000000, 1338);
}

void ParametrizedStress(size_t n) {
  vector<int> v(n);
  iota(v.rbegin(), v.rend(), 1);

  // https://www.cppstories.com/2016/11/iife-for-complex-initialization/
  const vector<int> expected = [n]{
    vector<int> v(n);
    iota(v.begin(), v.end(), 1);
    return v;
  }();

  MergeSort(v);

  ASSERT_EQUAL(v, expected);
}

void StressTest2() {
  ParametrizedStress(1100000);
}

void StressTest3() {
  ParametrizedStress(1500000);
}

void StressTest4() {
  ParametrizedStress(3000000);
}

void ToughTest() {
  const size_t n = 2'200'000'000;
  vector<char> v(n, 1);
  v.back() = 0;

  MergeSort(v);

  ASSERT_EQUAL(v.size(), n);
  ASSERT_EQUAL(v.front(), 0);
  ASSERT(std::all_of(v.begin() + 1, v.end(), [](char c) {return c == 1;}));
}

void RunAllTests() {
  using namespace std::chrono;
  testing::TestRunner tr;

  RUN_TEST(tr, CorrectnessTest0);
  RUN_TEST(tr, CorrectnessTest1);
  RUN_TEST(tr, CorrectnessTest2);

  RUN_TIMEOUT_TEST(tr, StressTest0, milliseconds(200));
  RUN_TIMEOUT_TEST(tr, StressTest1, milliseconds(200));
  RUN_TIMEOUT_TEST(tr, StressTest2, milliseconds(200));
  RUN_TIMEOUT_TEST(tr, StressTest3, milliseconds(300));
  RUN_TIMEOUT_TEST(tr, StressTest4, milliseconds(500));

  RUN_TIMEOUT_TEST(tr, ToughTest, seconds(300));
}

int main() {
  RunAllTests();
}
