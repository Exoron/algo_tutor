#include "CountingDouble.h"

CountingDouble::CountingDouble(double d): value{d} {}

CountingDouble& CountingDouble::operator=(const CountingDouble& d) {
  value = d.value;
  ++assignments;
  return *this;
}

#define DEFINE_CMP_OPERATOR(SIGN) \
  bool CountingDouble::operator SIGN(const CountingDouble& d) const { \
    return value SIGN d.value; \
  }

DEFINE_CMP_OPERATOR(==)
DEFINE_CMP_OPERATOR(!=)
DEFINE_CMP_OPERATOR(<)
DEFINE_CMP_OPERATOR(<=)
DEFINE_CMP_OPERATOR(>)
DEFINE_CMP_OPERATOR(>=)

double CountingDouble::Value() const {
  return value;
}

size_t CountingDouble::Assignments() const {
  return assignments;
}

std::ostream& operator<<(std::ostream& out, const CountingDouble& d) {
  return out << d.Value();
}