#pragma once

#include <cstdlib>
#include <iostream>

class CountingDouble {
 public:
  CountingDouble() = default;
  CountingDouble(double d);
  CountingDouble& operator=(const CountingDouble& d);

  bool operator==(const CountingDouble& d) const;
  bool operator!=(const CountingDouble& d) const;
  bool operator<(const CountingDouble& d) const;
  bool operator>(const CountingDouble& d) const;
  bool operator<=(const CountingDouble& d) const;
  bool operator>=(const CountingDouble& d) const;

  [[nodiscard]] double Value() const;
  [[nodiscard]] size_t Assignments() const;

 private:
  double value = 0;
  mutable size_t assignments = 0;
};

std::ostream& operator<<(std::ostream& out, const CountingDouble& d);