cmake_minimum_required(VERSION 3.15)
project(CountingDouble)

set(CMAKE_CXX_STANDARD 17)

add_library(LibCountingDouble STATIC CountingDouble.cpp)