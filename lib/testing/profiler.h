#pragma once

#include <chrono>
#include <iostream>
#include <string>

namespace testing {

class LogDuration {
 public:
  explicit LogDuration(const std::string &msg = "")
        : message(msg + ": "), start(std::chrono::steady_clock::now()) {
  }

  ~LogDuration() {
  auto finish = std::chrono::steady_clock::now();
  auto dur = finish - start;
  std::cerr << message
            << std::chrono::duration_cast<std::chrono::milliseconds>(dur).count()
            << " ms" << std::endl;
  }

 private:
  std::string message;
  std::chrono::steady_clock::time_point start;
};

class StopableTimer {
 public:
  StopableTimer() : total_time(0) {}

  void Start() {
    if (!active) {
      last_start = std::chrono::steady_clock::now();
      active = true;
    }
  }

  void Stop() {
    if (active) {
      total_time += std::chrono::duration_cast<std::chrono::nanoseconds>(
                      std::chrono::steady_clock::now() - last_start
                      );
      active = false;
    }
  }

  [[nodiscard]] std::chrono::milliseconds TotalTime() const {
    return std::chrono::duration_cast<std::chrono::milliseconds>(total_time);
  }

  [[nodiscard]] auto Milliseconds() const {
    return TotalTime().count();
  }

  void Print(const std::string &message) const {
    std::cerr << message << ": " << Milliseconds() << " ms" << std::endl;
  }

 private:
  std::chrono::steady_clock::time_point last_start;
  std::chrono::nanoseconds total_time;
  bool active = false;
};

} // namespace testing

#define UNIQ_ID_IMPL(lineno) _a_local_var_##lineno
#define UNIQ_ID(lineno) UNIQ_ID_IMPL(lineno)

#define LOG_DURATION(message) \
  testing::LogDuration UNIQ_ID(__LINE__){message};

