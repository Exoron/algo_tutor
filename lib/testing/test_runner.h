#pragma once

#include <sstream>
#include <stdexcept>
#include <iostream>
#include <map>
#include <unordered_map>
#include <set>
#include <string>
#include <vector>
#include <chrono>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>

#include "profiler.h"

namespace testing {

template<class T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& s) {
  os << "[";
  bool first = true;
  for (const auto& x: s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "]";
}

template<class T>
std::ostream& operator<<(std::ostream& os, const std::set<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x: s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template<class K, class V>
std::ostream& operator<<(std::ostream& os, const std::map<K, V>& m) {
  os << "{";
  bool first = true;
  for (const auto& kv: m) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class K, class V>
std::ostream& operator<<(std::ostream& os, const std::unordered_map<K, V>& m) {
  os << "{";
  bool first = true;
  for (const auto& kv: m) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class T, class U>
void AssertEqual(const T& t, const U& u, const std::string& hint = {}) {
  if (!(t == u)) {
    std::ostringstream os;
    os << "Assertion failed: " << t << " != " << u;
    if (!hint.empty()) {
      os << " hint: " << hint;
    }
    throw std::runtime_error(os.str());
  }
}

inline void Assert(bool b, const std::string& hint) {
  AssertEqual(b, true, hint);
}

class TestRunner {
 public:
  template<class TestFunc>
  void RunTest(TestFunc func, const std::string& test_name) {
    try {
      func();
      std::cerr << test_name << " OK" << std::endl;
    } catch (std::exception& e) {
      ++fail_count;
      std::cerr << test_name << " fail: " << e.what() << std::endl;
    } catch (...) {
      ++fail_count;
      std::cerr << "Unknown exception caught" << std::endl;
    }
  }

  // make separate entity (?)
  template<class TestFunc>
  void RunTimeoutTest(
      TestFunc func, const std::string& test_name,
      std::chrono::milliseconds time_limit) {
    std::atomic<bool> flag = false;
    std::condition_variable cv;
    std::mutex m;

    std::thread t([&] {
      RunTest(func, test_name);
      std::lock_guard guard(m);
      flag = true;
      cv.notify_one();
    });
    t.detach();

    bool in_time = false;
    {
      std::cerr << "Running test: " + test_name << std::endl;
      StopableTimer timer;
      timer.Start();

      auto wake_time = std::chrono::steady_clock::now() + time_limit;
      std::unique_lock lk(m);
      cv.wait_until(lk, wake_time, [wake_time, &flag, &in_time] {
        in_time = flag.load();
        return in_time || std::chrono::steady_clock::now() >= wake_time;
      });

      timer.Stop();
      std::cerr << timer.Milliseconds() << "ms / " <<
           time_limit.count() << "ms" << std::endl;
    }

    if (!in_time) {
      std::cerr << "Test timeout" << test_name << std::endl
                << ++fail_count << " unit tests failed. Terminate" << std::endl;
      std::exit(2);
    }
  }

  ~TestRunner() {
    if (fail_count > 0) {
      std::cerr << fail_count << " unit tests failed. Terminate" << std::endl;
      exit(1);
    }
  }

 private:
  std::atomic<int> fail_count = 0;
};

template<class T, class U>
void AssertLess(const T& t, const U& u, const std::string& hint = {}) {
  if (!(t < u)) {
    std::ostringstream os;
    os << "Assertion failed: " << t << " !< " << u;
    if (!hint.empty()) {
      os << " hint: " << hint;
    }
    throw std::runtime_error(os.str());
  }
}

template<typename T, typename U>
bool AreClose(const T& t, const U& u, const long double eps) {
  if (-eps < u && u < eps) {
    return -eps < t && t < eps;
  }
  return ((t - u) / u) < eps && ((u - t) / u) < eps;
}

template<typename T, typename U>
void AssertClose(
    const T& t, const U& u, const long double eps,
    const std::string& hint = {}) {
  if (!AreClose(t, u, eps)) {
    std::ostringstream os;
    os << "Assertion failed: " << "|" << t << " - " << u << "| / |" << u
       << "| > " << eps;
    if (!hint.empty()) {
      os << " hint: " << hint;
    }
    throw std::runtime_error(os.str());
  }
}

} // namespace testing

#define ASSERT_EQUAL(x, y) {            \
  std::ostringstream __os;                   \
  __os << #x << " != " << #y << ", "    \
    << __FILE__ << ":" << __LINE__;     \
  testing::AssertEqual(x, y, __os.str());        \
}

#define ASSERT(x) {                     \
  std::ostringstream __os;                   \
  __os << #x << " is false, "           \
    << __FILE__ << ":" << __LINE__;     \
  testing::Assert(x, __os.str());                \
}

#define RUN_TEST(tr, func) \
  tr.RunTest(func, #func)

#define ASSERT_LESS(x, y) {            \
  std::ostringstream __os;                   \
  __os << #x << " !< " << #y << ", "    \
    << __FILE__ << ":" << __LINE__;     \
  testing::AssertLess(x, y, __os.str());        \
}

#define ASSERT_CLOSE(x, y, eps) {                              \
  std::ostringstream __os;                                          \
  __os << "|" << #x << " - " << #y << "| / |" << #y << "| > " << eps << ", " \
    << __FILE__ << ":" << __LINE__;                            \
  testing::AssertClose(x, y, eps, __os.str());                          \
}

#define RUN_TIMEOUT_TEST(tr, func, timeout) \
  tr.RunTimeoutTest(func, #func, timeout)
