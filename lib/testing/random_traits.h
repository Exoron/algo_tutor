#pragma once

#include <vector>
#include <random>

namespace testing {

template <typename T>
struct DistrImpl;

#define DEFINE_DISCRETE_TYPE_DISTRIBUTION(TYPE) \
  template <> \
  struct DistrImpl<TYPE> { \
    using Distribution = std::uniform_int_distribution<TYPE>; \
  };

DEFINE_DISCRETE_TYPE_DISTRIBUTION(short)
DEFINE_DISCRETE_TYPE_DISTRIBUTION(int)
DEFINE_DISCRETE_TYPE_DISTRIBUTION(long)
DEFINE_DISCRETE_TYPE_DISTRIBUTION(long long)
DEFINE_DISCRETE_TYPE_DISTRIBUTION(unsigned short)
DEFINE_DISCRETE_TYPE_DISTRIBUTION(unsigned int)
DEFINE_DISCRETE_TYPE_DISTRIBUTION(unsigned long)
DEFINE_DISCRETE_TYPE_DISTRIBUTION(unsigned long long)

#define DEFINE_CONTINUOUS_TYPE_DISTRIBUTION(TYPE) \
  template <> \
  struct DistrImpl<TYPE> { \
    using Distribution = std::uniform_real_distribution<TYPE>; \
  };

DEFINE_CONTINUOUS_TYPE_DISTRIBUTION(float)
DEFINE_CONTINUOUS_TYPE_DISTRIBUTION(double)
DEFINE_CONTINUOUS_TYPE_DISTRIBUTION(long double)

template <typename T>
std::vector<T> GenerateVector(size_t n, T from, T to, int seed) {
  std::mt19937 gen(seed);
  typename DistrImpl<T>::Distribution distrib(from, to);

  std::vector<int> v;
  v.reserve(n);
  for (size_t i = 0; i < n; ++i) {
    v.push_back(distrib(gen));
  }
  return v;
}

} // namespace testing